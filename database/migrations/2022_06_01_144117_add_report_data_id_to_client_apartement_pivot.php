<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddReportDataIdToClientApartementPivot extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('client__apartments', function (Blueprint $table) {
            //
            $table->uuid('report_data_id')->nullable();
            $table->foreign('report_data_id')->references('id')->on('report_data');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('client__apartments', function (Blueprint $table) {
            $table->dropColumn('report_data_id');
        });
    }
}

<?php

use App\Models\Client;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateApartmentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('apartments', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->string('address');
            $table->integer('year_of_build');
            $table->integer('floor');
            $table->integer('area');
            $table->integer('rooms');
            $table->integer('total_floors');
            $table->integer('has_balcony');
            $table->integer('has_parking');
            $table->integer('has_lift');
            $table->integer('has_basement');
            $table->integer('has_light_kitchen');
            $table->string('standard');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('apartments');
    }
}

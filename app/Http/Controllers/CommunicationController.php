<?php

namespace App\Http\Controllers;

use App\Jobs\SendReportV2;
use Illuminate\Http\Request;
use App\Models\{Client, Apartment, Client_Apartment, ReportData};
use Illuminate\Support\Facades\Http;


class CommunicationController extends Controller
{

    public function getApartmentIDForWordPress(Request $request){
        $request = $request->all();
        $apartment_data = $this->getApartmentData($request);
        $apartment = new Apartment();
        $apartment_id = $apartment->upsertApartment($apartment_data);
        $this->updateApartmentInRemoteServer($apartment_data, $apartment_id, null);

        return response()->json(['apartment_id' => $apartment_id]);
    }

    public function receiveDataFromWordPress(Request $request){
        $request = $request->all();
        $client_data = $this->getClientData($request);
        $apartment_data = $this->getApartmentData($request);

        $client = new Client();
        $client_id = $client->upsertClient($client_data);

        $apartment = new Apartment();
        $apartment_id = $apartment->upsertApartment($apartment_data);

        $this->updateUsersInRemoteServer($client_data, $client_id);
        $this->updateApartmentInRemoteServer($apartment_data, $apartment_id, $client_id);

        $check = Client_Apartment::where('client_id', $client_id)->where('apartment_id', $apartment_id)->first();
        $report_data_id = null;
        if(!$check){
            $pivot = new Client_Apartment();
            $pivot->client_id = $client_id;
            $pivot->apartment_id = $apartment_id;
            $report_data = new ReportData();
            $report_data_id = $report_data->createRecord();
            $pivot->report_data_id = $report_data_id;
            $pivot->save();
        }
        else{
            $report_data_id = $check->report_data_id;
        }
        SendReportV2::dispatch($apartment_id, $client_id, $report_data_id);
        return response(['report_url' => route('pdf-salesforce', ['id' => $report_data_id])], 200);
    }

    public function updateApartmentInRemoteServer($apartment_data, $id, $client_id){
        $url = 'http://3.74.73.163:5000/apartments/' . $id;
        $data = [
            'parameters' => $apartment_data,
            'user_id' => $client_id
        ];
        $response = Http::put($url, $data);
    }

    public function updateUsersInRemoteServer($client_data, $id){
        $url = 'http://3.74.73.163:5000/users/' . $id;
        $response = Http::put($url, $client_data);
    }

    public function getClientData($data){
        $result = [];
        if(array_key_exists('name', $data))
            $result['name'] = $data['name'];

        if(array_key_exists('email', $data))
            $result['email'] = $data['email'];

        if(array_key_exists('mobile_number', $data))
            $result['phone'] = $data['mobile_number'];

        return $result;
    }

    public function getApartmentData($data){
        $result = [];

        if(array_key_exists('address', $data))
            $result['address'] = $data['address'];

        if(array_key_exists('year_of_build', $data))
            $result['year_of_build'] = $data['year_of_build'];

        if(array_key_exists('floor', $data))
            $result['floor'] = $data['floor'];

        if(array_key_exists('area', $data))
            $result['area'] = $data['area'];

        if(array_key_exists('rooms', $data))
            $result['rooms'] = $data['rooms'];

        if(array_key_exists('total_floors', $data))
            $result['total_floors'] = $data['total_floors'];

        if(array_key_exists('has_balcony', $data))
            $result['has_balcony'] = $data['has_balcony'];

        if(array_key_exists('has_parking', $data))
            $result['has_parking'] = $data['has_parking'];

        if(array_key_exists('has_basement', $data))
            $result['has_basement'] = $data['has_basement'];

        if(array_key_exists('has_lift', $data))
            $result['has_lift'] = $data['has_lift'];

        if(array_key_exists('has_light_kitchen', $data))
            $result['has_light_kitchen'] = $data['has_light_kitchen'];

        if(array_key_exists('property_condition', $data))
            $result['standard'] = $data['property_condition'];

        return $result;
    }
}

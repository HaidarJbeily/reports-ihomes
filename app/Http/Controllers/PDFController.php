<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Jobs\SendReport;

use Illuminate\Support\Facades\Log;

use PDF;
use Mailgun\Mailgun;
use Illuminate\Support\Facades\Http;
use App\Jobs\Chart;
use App\Models\ReportData;

class PDFController extends Controller
{
    //
    public function generatePDF(Request $request)
    {
        SendReport::dispatch($request->all());
        return response('' ,200);
    }

    public function generatePDF_Salesforce($id)
    {
        $report_data_model = new ReportData();

        $data = $report_data_model->getRecord($id);
        if(!$data){
            return view('not-available');
        }
        $googleApiKey = 'AIzaSyABEwhPOMYKKct52PCrdK0ljgtG3q8UOvI';

        $report_data = $data['statistics'];
        $localStandard = $data['standard'];
        $chart = new Chart();
        $bar1 = $chart->getBarChart1($report_data['standard_influence'], $localStandard);
        $bar2 = $chart->getBarChart2($report_data['district_m2price'], $report_data['poland_m2price'] , $report_data['district']);
        $bar3 = $chart->getBarChart3($report_data['m2price_per_year'], $data['year_build']);
        $line = $chart->getLineChart($report_data['ads_per_month']);

        $similar['similar'] = $data['similar'];
        $similar['original_index'] = $data['similar']['original_index'];


        $pdf = PDF::loadView('pdf2', ['data' => $data,
                                    'similar' => $similar,
                                    'currentData' => $data['date'],
                                    'entryId' => rand(2, 10000),
                                    'report_data' => $report_data,
                                    'bar1' => $bar1,
                                    'bar2' => $bar2,
                                    'bar3' => $bar3,
                                    'line' => $line,
                                    'googleAPIKey' => $googleApiKey,
                                    'address' => $data['address'],
                                    'localStandard' => $localStandard,
                                    ]);


        return $pdf->stream($data['address'].'pdf');
    }


    public function sendEmail(Request $request){
        $email = $request->all()['email'];

        $mgClient = Mailgun::create( '68ebc0982386c92f7c800fc2e1bc84b4-4b1aa784-6e1d2cdc', 'https://api.eu.mailgun.net' );
        $domain   = 'mg.ihomes.pl';
        $body = view('monthly-email')->render();
        $title= 'Monthly Email test';
        $params   = [
            'from'       => "iHomes <info@ihomes.pl>",
            'to'         => $email,
            'subject'    => $title ?? 'test',
            'html'       => $body
        ];

        $response = $mgClient->messages()->send( $domain, $params );
        $result = "";
        if ( $response->getMessage() == "Queued. Thank you." ) {

            $result = "success";
        } else {
            $result = "error";
        }
        return $result;
    }
}

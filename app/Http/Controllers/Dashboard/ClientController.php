<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use App\Models\Client;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;

class ClientController extends Controller
{

    public function index(){

        $data['page']='Clients';
        if (!isset($_GET['apartment'])){
            $data['clients']=Client::with('apartments')->paginate(20);

        }else{
            $data['clients']=Client::whereHas('apartments',function (Builder $q){
                $q->where('apartment_id',$_GET['apartment']);
            })->paginate(20);
        }

        return view('dashboard.client.index',$data);
    }
}

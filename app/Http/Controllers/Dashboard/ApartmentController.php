<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use App\Models\Apartment;
use App\Models\Client;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;

class ApartmentController extends Controller
{

    public function index(){

        $data['page']='Apartments';
        if (!isset($_GET['client'])){
            $data['apartments']=Apartment::with('clients')->paginate(20);

        }else{
            $data['client']=Client::findOrFail($_GET['client']);
            $data['apartments']=Apartment::whereHas('clients',function (Builder $q){
                $q->where('client_id',$_GET['client']);
            })->paginate(20);
        }
        return view('dashboard.apartment.index',$data);
    }
}

<?php
namespace App\Jobs;
use QuickChart;

class Chart{

    public function getBarChart1($data, $stand)
    {
        $chart = new QuickChart(array(
          'width' => 421,
          'height' => 221
        ));
          $blue = "'rgb(20, 120, 255)'";
          $dark = "'rgb(32, 44, 74)'";
          $colors = [];
          foreach ($data as $key => $value) {
            if($key == $stand)
            {
              array_push($colors, $dark);
            }
            else{
              array_push($colors, $blue);
            }
          }
          $colors = array_reverse($colors, true);
          $color_str = implode(',', $colors);

          $data_a = $data['Premium'];
          $data_b = $data['Dobry'];
          $data_c = $data['Przyzwoity'];
          $data_d = $data['Do remontu'];

          $refined = $data_d.','.$data_c.','.$data_b.','.$data_a;

          $max = max([$data_a, $data_b, $data_c, $data_d]);
          if ($max > 900000 )
          $max+=280000;
          else{
              $max+=150000;
          }
          $chart->setConfig(
          "{
                    type: 'bar',
                    data: {
                        labels: ['Do remontu', 'Przyzwoity', 'Dobry', 'Premium'],
                        datasets: [
                            {

                            backgroundColor: [".$color_str."],

                            borderWidth: 0,
                            data: [". $refined."],
                            },
                        ],
                    },
                    options: {
                        legend: {
                            display: false
                        },
                        scales: {
                            yAxes: [{
                                display: true,
                                gridLines: {
                                    display: true,
                                    drawBorder: true
                                    },
                                ticks: {
                                    display: false,
                                    max: ".$max.",
                                    beginAtZero: true
                                }
                            }],

                            xAxes: [{
                                gridLines: {
                                    display: false,
                                    drawBorder: false
                                    },
                                ticks: {
                                    fontSize: 12,

                                }
                            }]
                        },
                        tooltips: {
                            callbacks: {
                            label: function(tooltipItem) {
                                    return tooltipItem.yLabel;
                            }
                        }
                    },

                    plugins: {
                        datalabels: {
                            clamp: true,
                            anchor: 'end',
                            align: 'top',
                            color: '#444',
                            font: {
                                weight: 'medium',
                            },
                            formatter: (value) => {
                                return value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ' ')  + ' zł';
                            }
                        },
                    },
            },
        }");
          return base64_encode($chart->toBinary());
    }



    public function getBarChart2($district, $poland, $name)
    {
      $chart = new QuickChart(array(
        'width' => 100,
        'height' => 140
      ));

        $max = max([$district, $poland]);
        if ($max > 900000 )
          $max+=80000;
          else if ($max < 100000){
              $max+=4000;
          }
        $chart->setConfig(
        "{
          type: 'bar',
          data: {
            labels: ['".$name."', 'Warszawa'],
            datasets: [
              {

                backgroundColor: [ 'rgb(20, 120, 255)', 'rgb(20, 120, 255)'],
                borderColor: 'rgb(255, 99, 132)',
                borderWidth: 0,
                data: [".$district .", ".$poland."],
              },
            ],
          },
          options: {
            legend: {
                display: false
            },
            scales: {
                yAxes: [{
                    display: true,
                    gridLines: {
                        display: true,
                        drawBorder: true
                        },
                    ticks: {
                        display: false,
                        max:".$max.",
                        beginAtZero: true
                    }
                }],

                xAxes: [{
                    gridLines: {
                        display: false,
                        drawBorder: false
                        },
                    ticks: {
                        fontSize: 6,

                    }
                }]
            },
          tooltips: {
              callbacks: {
                 label: function(tooltipItem) {
                        return tooltipItem.yLabel;
                 }
              }
          },

            plugins: {
              datalabels: {
                clamp: true,
                anchor: 'end',
                align: 'end',
                color: '#444',
                formatter: (value) => {
                  return value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ' ')  + ' zł';
                },
                font: {
                  weight: 'normal',
                  size: 6
                },
              },
            },
          },
        }");
        return base64_encode($chart->toBinary());
    }



    public function getBarChart3($data, $year)
    {
      $chart = new QuickChart(array(
        'width' => 300,
        'height' => 200
      ));
      $max = -1;
      foreach ($data as $item)
      {
        if($max < $item[1])
          $max = $item[1];
      }

        $grey = "'rgb(20, 120, 255)'" ;
        $dark = "'rgb(32, 44, 74)'";

        $colors = array();
        $labels = array();
        $refined = array();

        $now = 0;
        foreach ($data as $item) {
          $first_arr = explode('-', $item[0]);
          if($now==0 || $now==5){
            array_push($labels, '"'.$item[0].'"');
          }
          else{
            array_push($labels, '"'.$first_arr[0][2].$first_arr[0][3].'-'.$first_arr[1][2].$first_arr[1][3].'"');
          }
          // array_push($labels, '"'.$now - intval($first_arr[1]).'-'.$now - intval($first_arr[0]).'"');
          array_push($refined, $item[1]);
          if(intval($first_arr[0])<= $year && intval($first_arr[1]) >= $year ){
            array_push($colors, $dark);
          }
          else{
            array_push($colors, $grey);
          }
          $now++;
        }

        $color_str = implode(',', $colors);
        $label_str = implode( ',',$labels);
        $refined_str = implode( ',', $refined);
        $chart->setConfig(
        "{
          type: 'bar',
          data: {
            labels: [".$label_str."],
            datasets: [
              {
                backgroundColor:  [".$color_str."],

                data: [".$refined_str."],
              },
            ],
          },
          options: {
            legend: {
              display: false
          },
          tooltips: {
              callbacks: {
                label: function(tooltipItem) {
                        return tooltipItem.yLabel;
                }
              }
          },
          scales: {
            yAxes: [{
              scaleLabel: {
                display: true,
                labelString: 'Cena m2'
              },
              ticks: {
                fontSize: 7,

              }

            }],
            xAxes: [{
              scaleLabel: {
                display: true,
                labelString: 'Rok budowy(w latach)'
              },
              ticks: {
                autoSkip: false,
                maxRotation: 90,
                minRotation: 90,
                fontSize: 7,

              }
            }]
          }  ,

          },
        }");
        return base64_encode($chart->toBinary());
    }


    public function getLineChart($data)
    {
      $chart = new QuickChart(array(
        'width' => 500,
        'height' => 300
      ));

        $labels = array();
        $refined = array();

        $translations = [
          'Mar' => 'Mar',
          'Apr' => 'Kwi',
          'May' => 'Maj',
          'Jun' => 'Cze',
          'Jul' => 'Lip',
          'Aug' => 'Sie',
          'Sep' => 'Wrz',
          'Oct' => 'Paź',
          'Nov' => 'Lis',
          'Dec' => 'Gru',
          'Jan' => 'Sty',
          'Feb' => 'Lut'
        ];


        foreach ($data as $item) {
          if($item[1] <= 0)continue;
          $splitted = explode(' ', $item[0]);
          if(array_key_exists($splitted[1], $translations))
            array_push($labels, '"'.$splitted[0]. ' '. $translations[$splitted[1]].'"');
          else
             array_push($labels, '"'.$splitted[0]. ' '. $splitted[1].'"');

          array_push($refined, $item[1]);
        }
        $label_str = implode( ',',$labels);
        $refined_str = implode( ',', $refined);

      $con = "{
        type: 'line',
        data: {
          labels: [".$label_str."],
          datasets: [
            {
              fill: false,
              borderColor: 'rgb(46, 163, 255)',
              tension: 0.1,

              data: [". $refined_str."],
            },
          ],
        },
        options: {
          legend: {
            display: false
        },
        tooltips: {
            callbacks: {
              label: function(tooltipItem) {
                      return tooltipItem.yLabel;
              }
            }
        },
        },
      }";

        $chart->setConfig($con);

        return base64_encode($chart->toBinary());
    }

}


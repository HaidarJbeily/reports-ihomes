<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

use Illuminate\Support\Facades\Log;

use PDF;
use Mailgun\Mailgun;
use Illuminate\Support\Facades\Http;
use App\Models\{Apartment, Client, ReportData};
use Illuminate\Support\Facades\DB;

class SendReportV2 implements ShouldQueue, ShouldBeUnique
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $timeout = 4000;

    public $apartment_id;
    public $client_id;
    public $report_data_id;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($apartment_id, $client_id, $report_data_id)
    {
        $this->apartment_id = $apartment_id;
        $this->client_id = $client_id;
        $this->report_data_id = $report_data_id;
    }


    public function deleteDuplicates(){
        $jobs = DB::table('jobs')->select('id', 'payload', 'attempts')->get();
        $compare = null;
        $data = [];
        foreach ($jobs as $job){
            $d = json_decode($job->payload, true)['data']['command'];
            if($job->attempts)
            {
                $compare = $d;
                continue;
            }
            if($d === $compare)
            {

                array_push($data, $job->id);
            }
        }
        if(!$data && count($data) > 0)
        {
            $id_str = implode(', ', $data);
            DB::delete('delete from jobs where id in (' . $id_str . ')');

        }
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $this->deleteDuplicates();
        $data = $this->prepareData();
        $this->updateReportDataTable($data);
        $googleApiKey = 'AIzaSyABEwhPOMYKKct52PCrdK0ljgtG3q8UOvI';

        $report_data = $data['statistics'];
        $localStandard = $data['standard'];
        $chart = new Chart();
        $bar1 = $chart->getBarChart1($report_data['standard_influence'], $localStandard);
        $bar2 = $chart->getBarChart2($report_data['district_m2price'], $report_data['poland_m2price'] , $report_data['district']);
        $bar3 = $chart->getBarChart3($report_data['m2price_per_year'], $data['year_build']);
        $line = $chart->getLineChart($report_data['ads_per_month']);

        $similar['similar'] = $data['similar'];
        $similar['original_index'] = $data['similar']['original_index'];


        $pdf = PDF::loadView('pdf2', ['data' => $data,
                                    'similar' => $similar,
                                    'currentData' => $data['date'],
                                    'entryId' => rand(2, 10000),
                                    'report_data' => $report_data,
                                    'bar1' => $bar1,
                                    'bar2' => $bar2,
                                    'bar3' => $bar3,
                                    'line' => $line,
                                    'googleAPIKey' => $googleApiKey,
                                    'address' => $data['address'],
                                    'localStandard' => $localStandard,
                                    ]);
        $this->sendEmail($data['email'], $pdf, $data['address']);
        $this->deleteDuplicates();
    }

    public function updateReportDataTable($data){
        $report_data = new ReportData();
        $report_data->updateRecord($this->report_data_id, $data);
    }

    public function prepareData(){

        $data_1 = Apartment::where('id', $this->apartment_id)->first()->toArray();
        $data_2 = Client::where('id', $this->client_id)->first()->toArray();
        $data_3 = $this->getPriceDataFromRemoteServer();
        $data_4 = $this->getReportDataFromRemoteServer();
        $data = array_merge($data_1, $data_2, $data_3, $data_4);
        $data['year_build'] = $data['year_of_build'];
        $data['date'] = date('m/d/Y');
        $data['apartment_floor'] = $data['floor'];
        $data['house_floor'] = $data['total_floors'];
        $data['rooms_number'] = $data['rooms'];
        $data['area_m2'] = $data['area'];
        $data['price'] = $data['price'];
        $data['price_m2'] = $data['m2price'];

        return $data;
    }

    public function getReportDataFromRemoteServer(){
        set_time_limit(4000);
        $url = 'http://3.74.73.163:5000/apartments/'.$this->apartment_id.'/report';
        $response = Http::timeout(40000)->get($url);
        return $response->json();
    }

    public function getPriceDataFromRemoteServer(){
        set_time_limit(4000);
        $url = 'http://3.74.73.163:5000/apartments/'.$this->apartment_id.'/price';
        $response = Http::timeout(40000)->get($url);
        return $response->json();
    }

    public function sendEmail($email, $pdf, $title ){
        $mgClient = Mailgun::create( '68ebc0982386c92f7c800fc2e1bc84b4-4b1aa784-6e1d2cdc', 'https://api.eu.mailgun.net' );
        $domain   = 'mg.ihomes.pl';
        $body = view('email')->render();
        $params   = [
            'from'       => "iHomes <info@ihomes.pl>",
            'to'         => $email,
            'subject'    => $title,
            'html'       => $body,
            'attachment' => [
                [ 'fileContent' => $pdf->output(), 'filename' => $title .'.pdf' ],
            ],
        ];

        $response = $mgClient->messages()->send( $domain, $params );
        $result = "";
        if ( $response->getMessage() == "Queued. Thank you." ) {

            $result = "success";
        } else {
            $result = "error";
        }
    }
}

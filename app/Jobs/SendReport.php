<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

use PDF;
use Mailgun\Mailgun;
use Illuminate\Support\Facades\Http;


class SendReport implements ShouldQueue, ShouldBeUnique
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $timeout = 4000;

    public $request;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($request)
    {
        //
        $this->request = $request;
    }

    public function deleteDuplicates(){
        $jobs = DB::table('jobs')->select('id', 'payload', 'attempts')->get();
        $compare = null;
        $data = [];
        foreach ($jobs as $job){
            $d = json_decode($job->payload, true)['data']['command'];
            if($job->attempts)
            {
                $compare = $d;
                continue;
            }
            if($d === $compare)
            {

                array_push($data, $job->id);
            }
        }
        if(!$data && count($data) > 0)
        {
            $id_str = implode(', ', $data);
            DB::delete('delete from jobs where id in (' . $id_str . ')');

        }
    }



    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $this->deleteDuplicates();
        //
        $data = [
            'title' => 'Welcome to ItSolutionStuff.com',
            'date' => date('m/d/Y')
        ];
        $r = $this->request;
        $similar = [];

        $data['apartment_floor'] = $r['floor'];
        $data['house_floor'] = $r['total_floors'];
        $data['rooms_number'] = $r['rooms'];
        $data['bright_kitchen'] = $r['has_light_kitchen'];
        $data['has_balcony'] = $r['has_balcony'];
        $data['has_lift'] = $r['has_lift'];
        $data['has_basement'] = $r['has_basement'];
        $data['has_parking'] = $r['has_parking'];
        $data['year_build'] = $r['year_of_build'];
        $data['address'] = $r['address'];
        // dd($data);
        set_time_limit(4000);
        Log::debug(print_r($r, true));
        $re = Http::timeout(3000)->post('http://3.74.73.163:5000/predict', $r);
        $re = $re->json();
        // Log::debug('response: '. $re);
        Log::info(print_r($re, true));
        // $coordinate = $re['locations'];
        $googleApiKey = 'AIzaSyABEwhPOMYKKct52PCrdK0ljgtG3q8UOvI';

        $report_data = $re['report'];
        $similar['similar'] = $re['similar'];
        $similar['original_index'] = $re['similar']['original_index'];
        $data['price'] = $re['price'];
        $data['price_m2'] = $re['m2price'];
        $data['area_m2'] = $r['area'];

        $entry = $r['eid'];
        $localStandard = $r['property_condition'];
        $chart = new Chart();
        $bar1 = $chart->getBarChart1($report_data['standard_influence'], $localStandard);
        $bar2 = $chart->getBarChart2($report_data['district_m2price'], $report_data['poland_m2price'] ?? 1 , $report_data['district']);
        $bar3 = $chart->getBarChart3($report_data['m2price_per_year'], $data['year_build']);
        $line = $chart->getLineChart($report_data['similar_ads']);

        $pdf = PDF::loadView('pdf', ['data' => $data,
                                    'similar' => $similar,
                                    'currentData' => $data['date'],
                                    'entryId' => $entry,
                                    'report_data' => $report_data,
                                    'bar1' => $bar1,
                                    'bar2' => $bar2,
                                    'bar3' => $bar3,
                                    'line' => $line,
                                    'googleAPIKey' => $googleApiKey,
                                    'address' => $r['address'],
                                    'localStandard' => $localStandard,
                                    // 'coordinate' => $coordinate,
                                    ]);
        $email = $r['email1'];
        $mgClient = Mailgun::create( '68ebc0982386c92f7c800fc2e1bc84b4-4b1aa784-6e1d2cdc', 'https://api.eu.mailgun.net' );
        $domain   = 'mg.ihomes.pl';
        $body = view('email')->render();
        $title= $data['address'];
        $params   = [
            'from'       => "iHomes <info@ihomes.pl>",
            'to'         => $email,
            'subject'    => $title ?? 'test',
            'html'       => $body,
            'attachment' => [
                [ 'fileContent' => $pdf->output(), 'filename' => $data['address'].'.pdf' ],
            ],
        ];

        $response = $mgClient->messages()->send( $domain, $params );
        $result = "";
        if ( $response->getMessage() == "Queued. Thank you." ) {

            $result = "success";
        } else {
            $result = "error";
        }
        $this->deleteDuplicates();
    }
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class Apartment extends Model
{
    use HasFactory;

    protected $casts = [
        'id' => 'string'
    ];

    protected $fillable = [
        'id',
        'address',
        'year_of_build',
        'floor',
        'area',
        'rooms',
        'total_floors',
        'has_balcony',
        'has_parking',
        'has_lift',
        'has_basement',
        'has_light_kitchen',
        'standard'
    ];

    public function upsertApartment($data){
        $res = $this->checkIfExists($data);
        if(!$res)
        {
            $data['id'] = (string) Str::uuid();;
            $res = $this->create($data);
            $res->id = $data['id'];
        }
        return $res->id;
    }

    public function checkIfExists($data){
        return $this->where($data)->first();
    }

    public function clients(){
        return $this->belongsToMany(Client::class,'client__apartments','apartment_id','client_id');
    }
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class Client extends Model
{
    use HasFactory;

    protected $casts = [
        'id' => 'string'
    ];

    protected $fillable = [
        'id',
        'name',
        'email',
        'phone'
    ];

    public function upsertClient($data){
        $res = $this->checkIfExists($data['email']);
        if(!$res)
        {
            $data['id'] = (string) Str::uuid();;
            $res = $this->create($data);
            $res->id = $data['id'];
        }
        else{
            $res->update($data);
        }
        return $res->id;
    }

    public function checkIfExists($email){
        return $this->where('email', $email)->first();
    }
    public function apartments(){
        return $this->belongsToMany(Apartment::class,'client__apartments','client_id','apartment_id');
    }
}

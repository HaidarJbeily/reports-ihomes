<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;


class ReportData extends Model
{
    use HasFactory;

    protected $casts = [
        'id' => 'string'
    ];

    protected $fillable = [
        'id',
        'data',
    ];

    public function createRecord(){
        $data = [
            'id' => (string) Str::uuid(),
        ];
        $this->create($data);
        return $data['id'];
    }

    public function updateRecord($id, $report_data){
        $this->where('id', $id)->update(['data' => json_encode($report_data)]);
    }

    public function getRecord($id){
        $res = $this->where('id', $id)->first();
        if($res)
            return json_decode($res->data, true);
        else
            return null;
    }
}

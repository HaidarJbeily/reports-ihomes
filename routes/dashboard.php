<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\PDFController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Auth::routes();
Route::group(['middleware' => ['auth']], function () {
    Route::get('/', [\App\Http\Controllers\Dashboard\HomeController::class, 'home'])->name('home');
    Route::get('index/{locale}', [App\Http\Controllers\Dashboard\HomeController::class, 'lang'])->name('dash.lang');
    Route::resource('clients', \App\Http\Controllers\Dashboard\ClientController::class);
    Route::resource('apartments', \App\Http\Controllers\Dashboard\ApartmentController::class);
//Route::get('login', [App\Http\Controllers\Dashboard\Auth\Auth::class, 'login']);
//Route::get('login', [App\Http\Controllers\Dashboard\Auth\Auth::class, 'login']);
});

<?php

use App\Http\Controllers\CommunicationController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\PDFController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('generate-pdf', [PDFController::class, 'generatePDF']);
Route::post('receive-data', [CommunicationController::class, 'receiveDataFromWordPress']);
Route::post('wp/apartment', [CommunicationController::class, 'getApartmentIDForWordPress']);


<?php
/**
 * @var  App\Modules\ProcessData\PDFSend $- >data
 * @var  App\Modules\ProcessData\PDFSend $- >coordinate
 * @var  App\Modules\ProcessData\PDFSend $- >currentData
 * @var  App\Modules\ProcessData\PDFSend $- >entryId
 */

 $translations = [
    'has_lift' => 'Winda',
    'has_basement' => 'Piwnica',
    'has_balcony' => 'Balkon',
    'has_parking' => 'Parking',
 ];
 $condition = [1 => "Premium",
2 => "Dobry",
3 => "Przyzwoty",
4 => "Do remontu"];
?>

<!DOCTYPE html>

<html lang="pl">
<head>
    <meta charset="utf-8">
    <title>{{ $address }}</title>
    <style>
        /* reset styles */
        html, body, div, span, applet, object, iframe,
        h1, h2, h3, h4, h5, h6, p, blockquote, pre,
        a, abbr, acronym, address, big, cite, code,
        del, dfn, em, img, ins, kbd, q, s, samp,
        small, strike, strong, sub, sup, tt, var,
        b, u, i, center,
        dl, dt, dd, ol, ul, li,
        fieldset, form, label, legend,
        table, caption, tbody, tfoot, thead, tr, th, td,
        article, aside, canvas, details, embed,
        figure, figcaption, footer, header, hgroup,
        menu, nav, output, ruby, section, summary,
        time, mark, audio, video {
            margin: 0;
            padding: 0;
            border: 0;
            /* color:#292A2B; */
            /* font: inherit; */
            /* font-size: 22px; */
            vertical-align: baseline;
        }
        @font-face {
                font-family: 'Proxima Nova';
                src:
                    url('/storage/fonts/proxima_nova_regular.ttf' ) format('truetype');
                    url( '/storage/fonts/proxima_nova_semibold.ttf' ) format('truetype');
                    url('storage/fonts/proxima_nova_extrabold.ttf') format('truetype');

            }
            /* @font-face {
                font-family: 'Proxima Nova';
                src:
                    url('/storage/fonts/proxima_nova_regular.ttf' ) format('truetype');
                font-weight: bold;
            }
            @font-face {
                font-family: 'Proxima Nova';
                src:
                    url( '/storage/fonts/proxima_nova_semibold.ttf' ) format('truetype');
                    font-weight: bolder;
            } */
/*
            @font-face {
                font-family: 'Proxima Nova';
                src:
                    url('storage/fonts/proxima_nova_extrabold.ttf') format('truetype');
                font-weight: bolder;
            }

            @font-face {
                font-family: 'Proxima Nova';
                src:
                    url('storage/fonts/proxima_nova_thin.ttf') format('truetype');
                    font-weight: 100,200,300;
            } */

        /* general styles*/
        *, *::before, *::after {
            /* -webkit-box-sizing: border-box; */
            /* box-sizing: border-box; */
        }

        html,
        body {
            max-width: 210mm;
            margin: 0 auto;
        }

        body {
            font-family: 'Proxima Nova', sans-serif;
            padding: 0;
            margin: 0;
            /* color: #3b3c41 !important; */
            position: relative;
            width: 100%;
            font-size: 20px;
            line-height: 1.5;
            border-top: 5px solid #2EA3FF;
        }

        @page {
            margin-bottom: 0 !important;
        }

        b {
            font-weight: 700;
        }

        img {
            max-width: 100%;
            height: auto;
            display: block;
        }

        .h2,
        .h3 {
            font-family:  'Proxima Nova', sans-serif;

        }

        .h2 {
            font-weight: bolder;
            color: #050505;
            font-size: 42px;
            line-height: 1.5;
            margin-bottom: 18px;
            text-transform: capitalize;
        }

        .h3 {
            font-weight: bold;
            color: #000000;
            font-size: 26px;
            line-height: 1.5;
            margin-bottom: 23px;
        }

        .h3--colored {
            color: #2EA4FB;
        }
        .hr_4{
            margin-left:1.6cm;
            width:17cm;
            margin-top:50px;
            margin-bottom:50px;
            margin-right:1.4cm;
            border: 4px dotted;
            border-style: none none dotted;
            color:#c9c9c9a8;
        }
        .hr_5{
            margin-top:90px;
            margin-bottom:90px;
            border: 7px dotted;
            border-style: none none dotted;
            color:#c9c9c9a8;
        }
        .clear::after {
            content: "";
            display: block;
            clear: both;
        }

        .left {
            float: left;
        }

        .right {
            float: right;
        }

        .container {
            width: 100%;
            padding: 0 0;
        }

        .divided-line {
            display: block;
            height: 1px;
            width: 100%;
            background-color: rgba(73, 85, 114, 0.4);
            margin: 50px 0 30px;
        }

        .row {
            margin: 0 -1%;
        }

        .col {
            margin: 0 1%;
        }

        .col-6 {
            width: 48%;
        }

        .col-7 {
            width: 56%;
        }

        .col-5 {
            width: 40%;
        }

        .col-4 {
            width: 31%;
        }

        .m-auto {
            margin: 0 auto;
        }

        .block {
            margin-bottom: 40px;
        }

        .block.block--price {
            text-align: center;
            background-color:#8888890a;
            margin-top: 145px;
            border: 1px solid #c9c9c94a;
            margin-right: 85px;
            padding:33px;
            color: #050505;
            font-size: 26px;
            font-weight: bold;
        }

        /* header */
        .header {
            padding: 30px 0 10px;
            color: #010101;
        }

        .header__wrapper {
            border-bottom: 1px solid rgba(73, 85, 114, 0.4);
            padding-bottom: 15px;
        }
        .page-break{
            page-break-before:always;
        }
        .logo {
            position: fixed;
            right: 160px;
            top:80px;
            display: inline-block;
            height: 0.8cm;
            width: auto;
            margin-top: -5px;
            margin-bottom: 5px;
        }

        .header p {
            margin-bottom: 0;
        }

        .header__data {
            font-size: 18px;
        }

        .header__data span {
            display: inline-block;
            margin-right: 65px;
        }

        .header__data span:last-of-type {
            margin-right: 0;
        }

        .main {
            padding: 153px 80px 0 0;
        }

        .main-info {
            color: #050505;
            font: 22px/39px Proxima Nova, sans-serif;
        }

        .address {
            font-weight: 700;
            font-size: 26px;
            margin-bottom: 23px;
            color: #292A2B;
        }

        .price {
            font-size: 55px;
            line-height: 1.1cm;
            font-family: 'Proxima Nova', sans-serif;

            margin-bottom: 16px;
            font-weight: bold;
        }

        .location,
        .comparison {
            page-break-inside: avoid;
        }
        .comparison{
            /* padding:2%; */
            margin-left:1%;
            padding-top:2.5cm;

        }
        .location {
            /* margin-bottom: 80px; */
            position:absolute;
            width:21cm;
            height:12.1cm;
        margin-left:0;
        }

        #map {
            margin-top: 10px;
        }

        #map img {
            /* width: 100%; */
            /* height: auto; */
            /* max-height: 480px; */

        }

        /* tables */
        .table table {
            width: 100%;
        }

        .table .name {
            text-transform: capitalize;
            color:#444;
        }

        .table .name,
        .table .value {
            white-space: nowrap;
        }

        .table1 {
            margin-left: -15px;
            font-weight: bold;
        }

        .table1 .value {
            color: #777;
        }

        .table1 table td {
            border: 1px solid #D5DDEB;
            border-radius: 4px;
            padding: 4px 15px 10px 15px;
            line-height: 26px;
        }

        .table3 {
            /* margin-top: 10px; */
            width:88%;
            padding-left:1cm;
        }

        .table3 table {
            border-collapse: collapse;
            color: #050505;
            text-align: left;
            font-family: 'Proxima Nova', sans-serif;

            font-size: 20px;
            line-height: 30px;
        }

        .table3 table th,
        .table3 table td {
            font-family: 'Proxima Nova', sans-serif;

            font-size: 20px;
            line-height: 26px;
        }

        .table3 thead {
            font-size: 20px;
            color: #2EA4FF;
            text-transform: capitalize;
            text-align: center;
        }

        .table3 thead th {
            padding: 20px 20px;
            font-weight: bold;
            text-align: center;
            vertical-align: bottom;

        }

        .table3 thead th.place {
            padding-left: 0;
        }

        .table3 tbody {
            font-size: 16px;
        }

        .table3 tbody td {
            padding: 10px 20px 15px 20px;
            text-align:center;
        }

        .table3 tbody tr {
            background-color: rgb(214,234,245);;
            border-radius: 4px;
        }

        .table3 tbody tr td:first-of-type {
            border-top-left-radius: 40px;
            border-bottom-left-radius: 40px;
            /* padding-left: 15px; */
            font-size: 19px;
            text-align:left;
            font-weight:bold;
        }

        .table3 tbody tr td:last-of-type {
            border-top-right-radius: 40px;
            border-bottom-right-radius: 40px;
            padding-right: 15px;
        }

        .table3 tbody tr .even {
            background-color: #FFFFFF;
        }

        .table3 tbody tr.active {
            background-color: rgb(255,210,210);



            font-weight: bolder;
        }
        .table3 tbody tr.active td {

font-weight: bolder;
            border: 2px 0px solid rgb(209,84,94);


        }
        .grey-italic-text {
            font-size: 20px;
            line-height: 1.5;
            /* font-style: italic; */
            color: #777;
            margin-bottom: 18px;
            font-weight: bold;
        }

        .no-wrap {
	        white-space: nowrap;
        }

        /* footer */
        .footer {
            font-size: 20px;
            line-height: 1.5;
            color: #000000;
            opacity: 1;
            padding: 38px 0 60px;
        }

        /* offer block */
        .offer-block {
            position: relative;
        }

        .offer-bg-img {
            background-color: #F4F6FA;
            width: 100%;
            height: auto;
        }
        .divider{
            /* page-break: */
        }
        .offer-button {
            position: relative;
            left: 35%;
            margin-top: -180px;
        }

        .offer-button a {
            font-weight: 700;
            display: inline-block;
            letter-spacing: 0;
            color: #FFFFFF;
            text-transform: uppercase;
            background-color: #F94646;
            /* box-shadow: 0 3px 6px #f9464640; */
            border-radius: 6px;
            width: 324px;
            padding: 18px 20px 20px;
            line-height: 1;
            text-align: center;
            text-decoration: none;
        }
        .note{
            margin-top:2cm;
            margin-left:0.8cm;
        }
        .note img{
            width:17%;
            height:auto;
            margin-left:2%;
        }
        .charts{
            margin-top:2.4cm;
            margin-left:0cm;

        }
        .p3-start-image{
            width:47%;
            height:0.7cm;
        }
        .p3-1-images{
            width:1cm;
            height:1cm;
        }
        .sec{
            height:1.4cm;
            width:auto;
        }
    </style>
</head>
<body>
<?php
use Illuminate\Support\Facades\Storage;
$i             = 0;
$data1          = file_get_contents( storage_path('images/logoPNG.png')) ;
$data_end          = file_get_contents( storage_path('images/end_p2.png')) ;
$data_note         = file_get_contents( storage_path('images/notanik.png')) ;
$data_negative       = file_get_contents( storage_path('images/negative.png')) ;
$data_p3_start        = file_get_contents( storage_path('images/p3_start.png')) ;
$data_p2_start        = file_get_contents( storage_path('images/p2_start.png')) ;
$data_p3_end         = file_get_contents( storage_path('images/p3_end.png')) ;
$data_positive         = file_get_contents( storage_path('images/positive1.png')) ;
$data_parameter = file_get_contents( storage_path('images/parameter_o.png'));
$data_location = file_get_contents( storage_path('images/local.png')) ;

$base64        = 'data:image/png;base64,' . base64_encode( $data1 );
$base64_end       = 'data:image/png;base64,' . base64_encode( $data_end );
$base64_note       = 'data:image/png;base64,' . base64_encode( $data_note );

$base64_positive       = 'data:image/png;base64,' . base64_encode( $data_positive );
$base64_negative       = 'data:image/png;base64,' . base64_encode( $data_negative );
$base64_p3_start       = 'data:image/png;base64,' . base64_encode( $data_p3_start );
$base64_p3_end       = 'data:image/png;base64,' . base64_encode( $data_p3_end );
$base64_p2_start       = 'data:image/png;base64,' . base64_encode( $data_p2_start );
$base64_parameter       = 'data:image/png;base64,' . base64_encode( $data_parameter );
$base64_local       = 'data:image/png;base64,' . base64_encode( $data_location);
// $report_data = $report_data;

?>

<img class="logo" src="<?php echo $base64; ?>" alt="logo">


<div class="main">
    <div class="container" style="position:relative;">
        <div class="main-info row clear" >
            <div class="col col-6 left" style="padding-left:2.1cm;">
                <div class="block">
                    <h2 class="h2 h3--colored">Wycena Nieruchomości</h2>
                    <p class="address">
                        <b  style="text-decoration:  underline; font-size:28px; "><?php echo $data['address'] ?? 'gdgdsg'; ?></b>
                    </p>
                    <p class="grey-italic-text" style="color:#777;font-size:22px;font-weight:bold;width:8.4cm;line-height:0.39cm;">Ta nieruchomość została wyceniona w oparciu o najnowsze dostępne dane
                        z rynku nieruchomości oraz zestaw parametrów mieszkania, które mają istotny
                        wpływ na cenę. Nasz algorytm bada korelacje między tymi zmiennymi i na
                        ich podstawie podaje szacowaną wartość mieszkania.
                    </p>
                </div>
            </div>
            <div class="col col-5 right">
                <div class="block block--price m-auto">
                    <h4 class="p2_title" style="font-weight:500;font-size:small; border-top:1px solid #c9c9c94a; border-bottom:1px solid #c9c9c94a;letter-spacing:0.05cm;color:#222;">WYCENA MIESZKANIA</h4>
                    <p class="price" style="margin:0;"><?php echo number_format( $data['price'] ?? 432351, 0, '', ' ' ); ?> zł </p>
                    <p style="color:#665;"><?php echo number_format( $data['price_m2'] ?? 323, 0, '', ' ' ); ?> zł/m2</p>
                </div>
            </div>
        </div>
        <hr class="hr_4" style="margin-bottom:0.4cm;margin-top:0.3cm;" >
        <div class="characteristics" style="padding-right:1cm;padding-left:2.1cm;">
            <img class="sec" src="<?php echo $base64_parameter ?>" alt="" style="width: 6.5cm;height:0.8cm;margin-bottom:0.4cm;margin-top:0.2cm;">
            <div class="table table1">
                <table style="width:100%; border-collapse: separate"
                       cellspacing="15">
                    <tbody>
                    <tr>
                        <td width="48%" class="clear">
                            <span class="name">Powierzchnia Użytkowa</span>
                            <span class="value right">
                            {{ $data['area_m2']}}  m2
                            </span>
                        </td>
                        <span style="width:0.3cm;" ></span>
                        <td width="48%" class="clear">
                            <span class="name">Piętro</span>
                            <span class="value right">
                             {{ $data['apartment_floor'] }}
								z
								 {{ $data['house_floor']  }}

                            </span>
                        </td>
                    </tr>
                    <span style="height:1cm;"></span>
                    <tr>
                        <td width="48%" class="clear">
                            <span class="name">Liczba Pokoi</span>
                            <span class="value right">
                            {{ $data['rooms_number'] }}

                            </span>
                        </td>
                        <span style="width:0.3cm;" ></span>

                        <td width="48%" class="clear">
                            <span class="name">Osobna Jasna Kuchnia</span>
                            <span class="value right">
                            <?php echo (  $data['has_light_kitchen'] == 1 ) ? 'Tak' : 'Nie'; ?>

                            </span>
                        </td>
                    </tr>
                    <span style="height:1cm;"></span>
                    <tr>
                        <td width="48%" class="clear">
                            <span class="name">Balkon / Taras</span>
                            <span class="value right">
                            <?php echo ( $data['has_balcony'] == 1) ? 'Tak' : 'Nie'; ?>


                            </span>
                        </td>
                        <span style="width:0.3cm;" ></span>

                        <td width="48%" class="clear">
                            <span class="name">Standard Lokalu</span>
                            <span class="value right">
                            <?php echo $localStandard ?? '1'; ?>
                            </span>
                        </td>
                    </tr>
                    <span style="height:1cm;"></span>
                    <tr>
                        <td width="48%" class="clear">
                            <span class="name">Winda</span>
                            <span class="value right">
                            <?php echo ( $data['has_lift'] == 1 ) ? 'Tak' : 'Nie'; ?>
                            </span>
                        </td>
                        <span style="width:0.3cm;" ></span>
                        <td width="48%" class="clear">
							<span class="name">
                                Piwnica / Komórka Lokatorska
                            </span>
                            <span class="value right">
                            <?php echo (  $data['has_basement'] == 1 ) ? 'Tak' : 'Nie'; ?>


                            </span>
                        </td>
                    </tr>
                    <span style="height:1cm;"></span>
                    <tr>
                        <td width="48%" class="clear">
                            <span class="name">Parking</span>
                            <span class="value right">
                            <?php echo ( $data['has_parking'] == 1 ) ? 'Tak' : 'Nie'; ?>


                            </span>
                        </td>
                        <span style="width:0.3cm;" ></span>
                        <td width="48%" class="clear">
                            <span class="name">Rok Budowy</span>
                            <span class="value right">
                            <?php echo $data['year_build'] ?>
                            </span>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>
        <hr class="hr_4" >

        <div class="location" style="border:0px solid;">
            <div class="row clear" style="padding-left:2.1cm;margin-bottom:0.4cm;">
                <div class="col col-6 left">
                    <img src="<?php echo $base64_local; ?>" alt="" class="sec" style="width: 4cm;height:0.8cm;margin-bottom:0.2cm;">
                    <p class="address name" style="margin-bottom: 0.6cm;"><b><?php echo $data['address']; ?></b></p>

                </div>
                <div class="col col-6"></div>
            </div>
            <div  style="width:20.8cm;height:9.5cm; border:0px solid;display:table">
                <div style="display:table-row;border:1px solid;">
                    <div style="width:2.03cm;height:8cm;display:table-cell;margin:0;padding:0;">
                        <div style="height:4cm;width:2.03cm;"></div>
                        <div style="background-color:rgb(214,224,235);height:4cm;width:2.03cm;"></div>
                    </div>
                    <div id="map" style="width:17cm;height:7cm;display:table-cell;">
                        <?php $index = $similar['similar']['original_index'];?>
                         <img style="width:17cm;height:8cm;" src="https://maps.googleapis.com/maps/api/staticmap?center=<?php echo $data['address']; ?>+PL&zoom=10.5&region=PL&language=pl
					&scale=2
					&size=500x220
					&format=png
					&markers=scale:2%7Cicon:https://ihomes.pl/wp-content/themes/ihomes/assets/img/pdf-map-markers/01-home_2.png%7C<?php echo $similar['similar']['properties'][$index]['location']['latitude'].','.$similar['similar']['properties'][$index]['location']['longitude']; ?>
                    <?php foreach ( $similar['similar']['properties'] as $key => $value ): ?>
					&markers=scale:2%7Canchor:center%7Cicon:https://ihomes.pl/wp-content/themes/ihomes/assets/img/pdf-map-markers/Circle_6.png%7Clabel:B%7C<?php echo $value['location']['latitude'].','.$value['location']['longitude']; ?>
					<?php endforeach; ?>
					&key=<?php echo $googleAPIKey ?? '1'; ?>" alt="google map">
                    </div>
                    <div style="width:1.03cm;height:8cm;display:table-cell;margin:0;padding:0;">
                        <div style="height:4cm;width:2.03cm;"></div>
                        <div style="background-color:rgb(214,224,235);height:4cm;width:2.03cm;"></div>
                    </div>
                </div>
                <div style="display:table-row;background-color:rgb(214,224,235);margin-top:0.1cm;">
                    <div style="background-color:rgb(214,224,235);height:2cm;width:2.03cm;display:table-cell;margin-top:0.1cm;padding-top:0.7cm;"> <a href="ihomes.pl" style="text-decoration:none;color:#222;margin-left:0.5cm;margin-top:4cm;border:0px solid;height:2cm;">IHOMES.PL</a></div>
                    <div  style="background-color:rgb(214,224,235);height:2cm;width:17cm;display:table-cell;margin-top:0.1cm;padding-top:0.7cm;">
                        <span class="right" style="color:#222">ID RAP </span>
                        <span class="right" style="width:0.3cm;"></span>
                        <span class="right" style="color:#222">DATE: <?php echo $currentData ?? '1'; ?></span>

                    </div>
                    <div style="background-color:rgb(214,224,235);height:2cm;width:2.03cm;display:table-cell;margin-top:0.1cm;padding-top:0.7cm;">
                    <span style="color:#222">
                        ORTU: <?php echo $entryId ?? '1'; ?>
                    </span>
                </div>
                </div>
            </div>
        </div>

       <div class="comparison">
            <div style="padding-left:1.7cm;">
                <img src="<?php echo $base64_p2_start; ?>" class= "sec"  style="width: 8cm;height:0.9cm;" alt="">
                <p class="grey-italic-text" style="font-weight:bold;margin-bottom:0.3cm;">Przykładowe oferty sprzedaży zaznaczone na mapie.</p>
            </div>
            <hr class="hr_4" style="margin-bottom:3px;" >
            <div class="table table3" style="margin-bottom:3px;padding-right:0.7cm;">
                    <table>
                        <thead class="thead" style="margin-bottom:0.5cm;">
                        <tr>
                            <th align="left" class="place" style="width:6.2cm;text-align:left;padding-left:0.4cm;">Adres Nieruchomości</th>
                            <th align="left" style="width:2cm;">Powierzchnia</th>
                            <th align="left" style="width:0.5cm;">Pokoje</th>
                            <th align="left" style="width:0.5cm;">Piętro</th>
                            <th align="left" style="width:0.5cm;">Standard</th>
                            <th align="left" style="width:0.5cm;">Cena</th>
                            <th align="left" style="width:0.5cm;">Cena m2</th>
                            <th align="left" style="width:1cm;">Dni w sprzedaży</th>
                        </tr>
                        </thead>
                        <tbody>

                        <?php foreach ( $similar['similar']['properties'] as $key => $value ): ?>
								<?php if ( $similar['original_index'] == $i ): ?>
                                    <tr class="active">
                                        <td ><?php echo $value['address']; ?></td>
                                <td ><?php echo $value['area']; ?></td>
                                <td ><?php echo $value['rooms']; ?></td>
                                <td ><?php echo $value['floor']; ?></td>
                                <td style="font-size: 15px;" ><?php echo $condition[$value['standard']]; ?></td>
                                <td><span class="no-wrap" ><?php echo number_format( $value['price'], 0, '', ' ' ); ?></span></td>
                                <td ><span class="no-wrap"><?php echo number_format( $value['m2price'], 0, '', ' ' ); ?></span></td>
                                <td ><?php echo $value['days_in_sell'] ?? "—"; ?></td>
                                </tr>
								<?php else: ?>
                                    <tr style="<?php echo "height:0.1cm;".(($i%2==0)?"":"background-color:rgb(245,246,251);"); ?>">
                                        <td ><?php echo $value['address']; ?></td>
                                <td class="grey-italic-text"><?php echo $value['area']; ?></td>
                                <td class="grey-italic-text"><?php echo $value['rooms']; ?></td>
                                <td class="grey-italic-text"><?php echo $value['floor']; ?></td>
                                <td style="font-size: 15px;" class="grey-italic-text"><?php echo $condition[$value['standard']]; ?></td>
                                <td class="grey-italic-text"><span class="no-wrap" ><?php echo number_format( $value['price'], 0, '', ' ' ); ?></span></td>
                                <td class="grey-italic-text"><span class="no-wrap" ><?php echo number_format( $value['m2price'], 0, '', ' ' ); ?></span></td>
                                <td class="grey-italic-text"><?php echo $value['days_in_sell'] ?? "—"; ?></td>
                                </tr>
								<?php endif; ?>

								<?php $i ++; ?>
                                <tr style="background-color:#fff">
                                <td style="height:0.05cm;"> </td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td><span class="no-wrap"></span></td>
                                <td><span class="no-wrap"></span></td>
                                <td></td>
                            </tr>
							<?php endforeach; ?>
                        </tbody>
                    </table>

            </div>
        </div>

        <div style="padding:1.7cm;">
            <img style="width:14cm; height:auto; " src="<?php echo $base64_end; ?>" alt="">
        </div>
    </div>
    <footer class="footer" style="position:fixed;bottom:0px;width:100%;margin-left:0%; height:0.3cm;padding-left: 1.3cm;background-color:rgb(46,163,255)">

        <div class="col-5 left">
            <a href="https://ihomes.pl/pl/" style="text-decoration: none;color:#FFF;font-weight:500;">IHOMES.PL</a>
        </div>
        <div class="col-5 right" style="color:#FFF;padding-left:0.2cm;">
            <span style="font-weight:500;">DATE:</span> <span class="grey-italic-text" style="color: #fff"><?php echo $currentData; ?></span>
            <span style="width:0.3cm;"></span>
            <span style="font-weight:500;padding-left:0.2cm;">RAPORTU ID:</span>
         <span class="grey-italic-text" style="color: #fff"><?php echo $entryId; ?></span>
        </div>

    </footer>
    <div class="page-break"></div>
    <div class="charts" style="">
        <div style="padding-left:1.8cm;margin-bottom:0.5cm;">
            <img style="width: 10cm;height:0.9cm;" class="p3-start-image" src="<?php echo $base64_p3_start;?>" alt="">
        </div>

        <div class="col-9 left" style=" height:10cm; width:20cm;padding:0;margin:0;" >
            <div class=" left" style="width:34%;border-right:1px solid #CCC; margin-left:1cm;height:6.5cm;padding-left:1cm;">
                <h3 class="h3 h3--colored"  style="margin-bottom: 0.1cm;">
                Szczegóły mieszkania
                </h3>
                <p style="line-height:0.3cm;margin-bottom:0.4cm;color:#aaa;font-size:20px;" class="grey-italic-text">Mieszkania posiadające poniższe atuty, są średnio droższe o:</p>
                <table style="width:100%;">
                    <tbody>
                            <?php foreach ($report_data['additional_parameters'] as $key => $value): ?>
                                <tr width="100%" style="width:100%;">
                                    <td >
                                        <?php if (  $data[$key] == 0 ): ?>
                                            <img style="width: 0.85cm;height:0.85cm;" class="p3-1-images" src="<?php echo $base64_negative ?>" alt="">
                                        <?php endif; ?>
                                        <?php if (  $data[$key] == 1): ?>
                                            <img style="width: 0.85cm;height:0.85cm;" class="p3-1-images" src="<?php echo $base64_positive ?>" alt="">
                                        <?php endif; ?>
                                    </td>
                                    <td  class="" style="height:1cm;padding-right:1cm;padding-top:0.09cm;">
                                    <span class="h4"  style="font-weight:200;font-size:small;color:#444;"><?php echo $translations[$key]; ?></span> <span  style="margin-top:0.4cm;"><?php echo ' +'. (($value<0)?-1*$value:$value); ?>%</span>
                                    </td>
                                </tr>
                                <tr>
                                        <td style="height:0.1cm;"> </td>
                                        <td></td>

                                    </tr>
                            <?php endforeach; ?>
                        </tbody>
                    </table>
                </div>
                <div class="right" style="margin-right:1.5cm;width:9cm;margin-left:2cm;border: 0px solid;">
                    <h3 class="h3 h3--colored" style="margin-bottom:0.1cm;">
                        Standard mieszkania
                    </h3>
                    <p style="width:96%; margin-bottom:0.4cm;line-height:0.3cm;margin-top:0cm;color:#aaa;font-size:20px;" class="grey-italic-text">
                    Standard wycenianego mieszkania to: <span style="font-weight:bold;color:#333;"> <?php echo $localStandard?? 'Dobry'; ?></span>. Remont mieszkania może
                    podnieść jego wartość o <span style="font-weight:bold;color:#333;"><?php echo $report_data["renovation"] ?? 1; ?> zł/m2 </span>.
                    </p>
                    <div style="height:4.6cm;width:100%;border: 0px solid;" >
                        <img style="height:4.6cm;width:100%;max-width:100%;" src="<?php echo 'data:image/png;base64,' .$bar1; ?>" alt="">
                    </div>
                </div>

                    <hr class="hr_4" style="margin-top:7.5cm; margin-left:1.8cm; border: 0.01cm solid #DDD;width:17cm;">
                    <div style="margin-top:1cm;margin-left:2.1cm;">
                        <div class="col-4 left" style="margin-top:0.6cm;">
                            <h3 class="h3 h3--colored" style="margin-bottom:0.1cm;">
                                Rok budowy budynku
                            </h3>
                            <p style="width:96%; margin-bottom:0.4cm;line-height:0.3cm;margin-top:0cm;color:#aaa;font-size:20px;" class="grey-italic-text">
                            Rok powstania budynku jest jest
                                jednym z istotnych czynników
                                kształtujących cenę mieszkania.
                                </p>
                        </div>
                    <div class=" right" style="width:10cm;height:4cm;margin-right:10%;">
                        <img style="width:10cm;height:4cm;" src="<?php echo 'data:image/png;base64,' .$bar3 ?>" alt="">
                    </div>
                    <hr class="hr_4" style="margin-top:4.8cm; margin-left:0.13cm;width:90%;">

                </div>
                <div class="col-9 left" style="padding-left:1.8cm;border: 0px solid;">
                    <table style="margin:0; height:1.1cm;width:8.5cm;" >
                        <thead>
                            <tr>
                                <th>
                                    <img  style="width: 8cm;height:0.9cm;margin-bottom:0.4cm;" src="<?php echo $base64_p3_end; ?>" alt="">
                                </th>
                            </tr>
                        </thead>
                    </table>

                    <div class="col-5 left" style="border-right:1px solid #CCC; margin-left:0.4cm;height:8cm; padding-right:0.5cm;width:31%;">
                        <h3 class="h3 h3--colored" style="margin-bottom:0.1cm;">
                        Dla najbliższej okolicy
                        </h3>
                        <p style="width:96%; margin-bottom:0.4cm;line-height:0.3cm;margin-top:0cm;color:#aaa;font-size:19px;" class="grey-italic-text">
                            Przewidywana cena za m2 dla okolicy
                            <span style="font-weight:bold;color:#444;"><?php echo $report_data["district"] ; ?></span> to <span style="font-weight:bold;color:#444;"> <?php echo $report_data["district_m2price"]; ?></span> zł.
                            Przewidywany średni czas wygaśnięcia
                            oferty sprzedaży to <span style="font-weight:bold;color:#444;"><?php echo $report_data["district_expiration"]; ?></span> dni.
                            Od początku roku cena za m2 dla rejonu
                            <span style="font-weight:bold;color:#444;"><?php echo $report_data["district"]; ?></span> zmieniła się o <span style="font-weight:bold;color:#777;"><?php echo $report_data["district_m2price_change"].'%'; ?></span>
                        </p>
                        <img style="height:4.8cm;width:5.4cm;" src="<?php echo 'data:image/png;base64,' .$bar2 ?>" alt="">

                    </div>
                    <div class="col-6 right" style="margin-right:7%;margin-left:0cm;border:0px solid;width:54%;">
                        <h3 class="h3 h3--colored">
                        Liczba podobnych ofert
                        </h3>

                        <img style="height:7cm;width:16cm;" src="<?php echo 'data:image/png;base64,' .$line ?>" alt="">
                    </div>
                </div>
                <table style="border: 1px solid;width:23cm;height:1.53cm;margin-right:1cm;padding:0;margin-top:10.2cm;background-color:rgb(46,163,255);border:none;">
                    <tbody style="color:#FFF;">
                        <td width="16%" style="vertical-align:middle;padding-top:0.4cm;">
                           <a href="https://ihomes.pl/pl/" style="text-decoration: none;color:#FFF; margin-left:1.1cm;margin-top:1cm;font-weight:500;">IHOMES.PL</a>

                        </td>
                        <td width="40%">
                        </td>
                        <td width="37%" style="vertical-align:middle;padding-top:0.4cm;">
                            <span style="font-weight:500;">
                                DATE:
                            </span>

            <span class="grey-italic-text" style="color: #fff"><?php echo $currentData; ?></span>

                            <span style="width:0.3cm;"></span>

                            <span style="font-weight:500;">
                                RAPORTU ID:
                            </span>
                            <span class="grey-italic-text" style="color: #fff"><?php echo $entryId; ?></span>
                        </td>
                    </tbody>
                </table>
            </div>

        </div>

    <div class="page-break">
    </div>
    <div class="note">
        <img src="<?php echo $base64_note ?>" alt="">
        <hr class="hr_5" >
        <hr class="hr_5" >
        <hr class="hr_5" >
        <hr class="hr_5" >
        <hr class="hr_5" >
        <hr class="hr_5" >
        <hr class="hr_5" >
        <hr class="hr_5" >
        <hr class="hr_5" >
        <hr class="hr_5" >
        <hr class="hr_5" >
        <hr class="hr_5" >
        <hr class="hr_5" >
        <hr class="hr_5" >
        <hr class="hr_5" >
        <hr class="hr_5" >
        <hr class="hr_5" >
    </div>
    <footer class="footer" style="position:fixed;bottom:0px;width:100%;margin-left:0%;padding:0;padding-top:0.4cm; height:1.2cm;padding-left: 1.3cm;background-color:rgb(46,163,255)">


                                            <span></span>
        <div class="col-5 left">
            <a href="https://ihomes.pl/pl/" style="text-decoration: none;color:#FFF;font-weight:500;">IHOMES.PL</a>
        </div>
        <div class="col-5 right" style="color:#FFF;padding-left:0.2cm;">
            <span style="font-weight:500;">DATE:</span>  <span class="grey-italic-text" style="color: #fff"><?php echo $currentData; ?></span>
            <span style="width:0.3cm;"></span>
            <span style="font-weight:500;padding-left:0.2cm;">RAPORTU ID:</span>
            <span class="grey-italic-text" style="color: #fff"><?php echo $entryId; ?></span>
        </div>


</footer>
</div>
</body>
</html>

@extends('dashboard.layouts.master')
@section('title') @lang('translation.dashboards') @endsection
@section('css')
    <link href="{{ URL::asset('assets/libs/sweetalert2/sweetalert2.min.css') }}" rel="stylesheet" type="text/css" />
@endsection
@section('content')
@component('dashboard.components.breadcrumb')
@slot('li_1') Dashboards @endslot
@slot('title') {{$page}} @endslot
@endcomponent
<div class="row">
    <div class="col">

        <div class="h-100">
            <div class="row">


                <div class="col-lg-12">
                    <div class="card">

                        <div class="card-body">
                            <div id="customerList">
                                <div class="row g-4 mb-3">
                                    <div class="col-sm-auto">

                                    </div>
                                    <div class="col-sm">
                                        <div class="d-flex justify-content-sm-end">
                                            <div class="search-box ms-2">
                                                <input type="text" class="form-control search" placeholder="Search...">
                                                <i class="ri-search-line search-icon"></i>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="table-responsive table-card mt-3 mb-1">
                                    <table class="table align-middle table-nowrap" id="customerTable">
                                        <thead class="table-light">
                                        <tr>
                                            <th class="sort" data-sort="customer_name">Name</th>
                                            <th class="sort" data-sort="email">Email</th>
                                            <th class="sort" data-sort="phone">Phone</th>
                                            <th class="sort" data-sort="status">Apartments</th>
                                        </tr>
                                        </thead>
                                        <tbody class="list form-check-all">
                                        @foreach($clients as $client)
                                        <tr>
                                            <td class="customer_name">{{$client->name}}</td>
                                            <td class="email">{{$client->email}}</td>
                                            <td class="phone">{{$client->phone}}</td>
                                                <td class="status">
                                                    @if(count($client->apartments)>0)
                                                    <a href="{{route('apartments.index').'?client='.$client->id}}" class="badge badge-soft-success text-uppercase"> ({{count($client->apartments)}}) Show</a>
                                                        @endif
                                                </td>

                                        </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                    <div class="noresult" style="display: none">
                                        <div class="text-center">
                                            <lord-icon src="https://cdn.lordicon.com/msoeawqm.json" trigger="loop"
                                                       colors="primary:#121331,secondary:#08a88a" style="width:75px;height:75px">
                                            </lord-icon>
                                            <h5 class="mt-2">Sorry! No Result Found</h5>
                                        </div>
                                    </div>
                                </div>

                                <div class="d-flex justify-content-end">
                                    <div class="pagination-wrap hstack gap-2">

                                        @if($clients->currentPage()!=1)
                                        <a class="page-item pagination-prev disabled"
                                           href="{{$clients->path().'?page='.($clients->currentPage()-1)}}">
                                            Previous
                                        </a>
                                        @endif
                                        <ul class="pagination listjs-pagination mb-0"></ul>
                                            @if($clients->lastPage()!=$clients->currentPage())
                                        <a class="page-item pagination-next"
                                           href="{{$clients->path().'?page='.($clients->currentPage()+1)}}">
                                            Next
                                        </a>
                                                @endif
                                    </div>
                                </div>
                            </div>
                        </div><!-- end card -->
                    </div>
                    <!-- end col -->
                </div>


            </div> <!-- end row-->


        </div> <!-- end .h-100-->

    </div> <!-- end col -->


</div>

@endsection
@section('script')
    <script src="{{ URL::asset('assets/libs/prismjs/prismjs.min.js') }}"></script>
    <script src="{{ URL::asset('assets/libs/list.js/list.js.min.js') }}"></script>
    <script src="{{ URL::asset('assets/libs/list.pagination.js/list.pagination.js.min.js') }}"></script>

    <!-- listjs init -->
    <script src="{{ URL::asset('assets/js/pages/listjs.init.js') }}"></script>

    <script src="{{ URL::asset('assets/libs/sweetalert2/sweetalert2.min.js') }}"></script>
    <script src="{{ URL::asset('/assets/js/app.min.js') }}"></script>
@endsection

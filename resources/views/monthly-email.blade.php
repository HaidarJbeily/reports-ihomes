
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:o="urn:schemas-microsoft-com:office:office"><head>
    <!--[if gte mso 9]><xml>
        <o:OfficeDocumentSettings>
            <o:AllowPNG/>
            <o:PixelsPerInch>96/o:PixelsPerInch
                /o:OfficeDocumentSettings
    </xml><![endif]-->
    <meta charset="UTF-8">
    <title></title>
    <!--[if !mso]>-->
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!--<![endif]-->
    <meta name="format-detection" content="telephone=no">
    <meta name="viewport" content="width=device-width, initial-scale=1.0 maximum-scale=1.0; user-scalable=no;">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <style>
        @font-face {
                font-family: 'Proxima Nova';
                src:
                    url('/storage/fonts/proxima_nova_regular.ttf' ) format('truetype');
                    url( '/storage/fonts/proxima_nova_semibold.ttf' ) format('truetype');
                    url('storage/fonts/proxima_nova_extrabold.ttf') format('truetype');

            }
        .backgroundTable{margin:0 auto;padding:0;width:100%!important;}
        .ReadMsgBody{width:100%;}
        .ExternalClass{width:100%;}
        .ExternalClass,.ExternalClass p,.ExternalClass span,.ExternalClass font,.ExternalClass td,.ExternalClass div{line-height: 100%;}
        body{-webkit-text-size-adjust:100%;-ms-text-size-adjust:100%;width:100%!important;margin:0;padding:0;-webkit-font-smoothing:antialiased!important;}
        table{mso-table-lspace:0pt;mso-table-rspace:0pt;}
        table td{border-collapse: collapse;mso-table-lspace:0pt;mso-table-rspace:0pt;padding:0;}
        img{-ms-interpolation-mode:bicubic;}
        a img{border:none;}
        p{Margin: 0;}
        .vl {

        }
        .hr_4{
            /* #c9c9c94a */
            width:100%;
            /* margin-left:1.6cm; */
            margin-top:5px;
            margin-bottom:5px;
            /* margin-right:1.4cm; */
            border: 1px solid;
            border-style: solid;
            color:#c9c9c9a8;
        }
        .p{
            font-family: 'Proxima Nova', sans-serif;
        }
        @media only screen and (max-width: 480px){
            .sm-pl-18{padding-left:18px!important;}
            .sm-pr-18{padding-right:18px!important;}
            .sm-pt-0{padding-top:0!important;}
            .sm-pt-26{padding-top:26px!important;}
            .sm-w-46{width:46px!important;max-width:46px!important;}
            .sm-txt{line-height:auto!important;}
            .sm-txt span{line-height:24px!important;}
            .sm-stack{display:table!important;width:100%!important;}
            .sm-left{float:left!important;}
            .sm-w-100pc{width:100%!important;}
            .center{margin:0 auto!important;}
            .txt-center p{text-align:center!important;}
            .price {
                font-size: 55px;
                font-weight: bold;
                /* line-height: 1.1cm; */
                /* font-family: 'Proxima Nova', sans-serif; */

                margin-bottom: 16px;
        }
        }
    </style>
</head>
<body leftmargin="0" marginwidth="0" topmargin="0" marginheight="0" offset="0">
<table width="100%" cellspacing="0" cellpadding="0" border="0">
    <tbody><tr>
        <td>
            <!--[if mso]><table width="660" cellspacing="0" cellpadding="0" border="0" align="center"><![endif]--><!--[if !mso]><!-->
            <table cellspacing="0" cellpadding="0" border="0" align="center" style="width:100%;max-width:660px;"><!--<![endif]-->
                <tbody><tr>
                    <td>
                    <table width="100%" cellspacing="0" cellpadding="0" border="0">
                        <tbody>
                            <tr>
                                <td class="sm-pl-18 sm-pr-18" style="padding:16px 40px;background-color:#2EA4FB;">
                                    <div dir="rtl" style="display:table;width:100%;">

                                        <div  class="sm-stack txt-center" style="display:table-cell;vertical-align:middle;height:100px;">
                                            <p style="font:24px 'Proxima Nova', sans-serif;color:#FFFFFF;line-height:30px;text-align:center;font-size:24px;"><span style="font-size:40px;font-weight:bold;font-family:'Proxima Nova', sans-serif;">iHomes</span> <i class="fa fa-home" style="font-size:40px;" aria-hidden="true"></i></p>
                                        </div>
                                        <!--[if mso]></td></table><![endif]-->
                                    </div>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    <table width="100%" cellspacing="0" cellpadding="0" border="0">
                        <tbody>
                            <tr>
                                {{-- #F4F6FA; --}}
                                <td class="sm-pl-18 sm-pr-18" style="padding:54px 40px;background-color:#FFF;">
                                    <table width="100%" cellspacing="0" cellpadding="0" border="0">
                                        <tbody><tr>
                                            <td style="font:16px 'Proxima Nova', sans-serif;color:#333333;line-height:32px;">
                                                <p style="font:bold 28px 'Proxima Nova', sans-serif;color:#2EA4FD;line-height:30px;padding:0px 0px 0px 0px;text-align:center;">
                                                    Dzień dobry!
                                                </p>
                                                <hr class="hr_4" style="margin-bottom:20px;">
                                                <p style="text-align: center;font-size:20px; color:#555;line-height:28px;">
                                                    Przedstawiamy aktualizację wartości wycenionego przez Ciebie
                                                    mieszkania. Poniżej znajduje się aktualna wycena wykonana w
                                                            oparciu o dane z minionego miesiąca.
                                                </p>
                                                <hr class="hr_4" style="margin-top:20px;">

                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                        <table width="100%" cellspacing="0" cellpadding="0" border="0">
                            <tbody><tr>
                                <td class="sm-pl-18 sm-pr-18" style="padding:54px 40px 23px 40px;font:bold 24px 'Proxima Nova', sans-serif;color:#000000;padding-top:0;">
                                    <div style="background-color:#F4F6FA; border-radius:70px;vertical-align:middle;padding-top:10px;padding-bottom:10px;height:60px; ">
                                        <p style="font:bold 20px 'Proxima Nova', sans-serif;color:#2EA4FD;line-height:30px;padding:0px 0px 0px 0px;text-align:center;margin:0;">
                                        ulica Józefa Mianowskiego
                                        </p>
                                        <p style="font: 18px 'Proxima Nova', sans-serif;color:#222;line-height:30px;padding:0px 0px 0px 0px;text-align:center;margin:0;">
                                            Mokotów
                                            </p>
                                    </div>
                                </td>
                            </tr>
                            </tbody></table>
                        <table width="100%" cellspacing="0" cellpadding="0" border="0">
                            <tbody><tr>
                                <td class="sm-pl-18 sm-pr-18" style="padding:19px 40px;">
                                    <table width="100%" cellspacing="0" cellpadding="0" border="0">
                                        <tbody><tr>
                                            <td class="sm-w-46" width="64" valign="top" align="center" >
                                                <hr class="hr_4" style="margin:0px;width:190px;margin-bottom:2px;">
                                               <span style="letter-spacing: 3px;font-weight:bold;font-size:18px;font-family:'Proxima Nova', sans-serif;">Aktualna wartość</span>
                                                <hr class="hr_4" style="margin:0px;width:190px;margin-top:2px;">
                                                <p style="font-size: 36px;margin-top:7px;
                                                font-weight: bold;font-family:'Proxima Nova', sans-serif;margin-bottom:1px;color:#222;">123 456 zł</p>
                                                <span style="font-size: 17px;margin-top:7px;
                                                font-weight: bold;color:#666;font-family:'Proxima Nova', sans-serif;">-5% (w ost.msc)</span>
                                            </td>

                                        </tr>
                                        </tbody></table>
                                </td>
                            </tr>
                            </tbody></table>
                        <table width="100%" cellspacing="0" cellpadding="0" border="0">
                            <tbody><tr>
                                <td class="sm-pl-18 sm-pr-18" style="padding:19px 40px 58px 40px;font:16px Helvetica,Arial,sans-serif;color:#333333;line-height:32px;" align="center">

                                                <hr class="hr_4" style="margin:15px 0 40px 0;">
                                                <p style="font:bold 20px    'Proxima Nova', sans-serif;color:#2EA4FD;line-height:30px;padding:0px 0px 0px 0px;text-align:center;">
                                                    Ostatnio sprzedane mieszkania
                                                    </p>
                                                    <p style="color: #999; font-weight:bold; margin-top:4px;font-family:'Proxima Nova', sans-serif;">Mokotów</p>
                                                    <table style="width: 100%;">
                                                        <tbody style="width:100%;">
                                                            <tr width="100%" dir="ltr">
                                                                <td width="51%" align="center"  class="sm-pl-18 sm-pr-18">
                                                                        <p style="font-family:'Proxima Nova', sans-serif;font-size: 30px;margin-top:7px;
                                                                        font-weight: bold;margin-bottom:1px;">10</p>
                                                                        <p style="font-family:'Proxima Nova', sans-serif;font-size: 17px;margin-top:7px;
                                                                        font-weight: bold;color:#666;">Sprzedane w okolicy</p>
                                                                </td>
                                                                <td width="1%" align="center">
                                                                    <div class="vl" style="margin-right:10px;border-left: 1px solid #c9c9c9a8;
                                                                    height: 100px;"></div>
                                                                </td>
                                                                <td width="49%" align="center">
                                                                    <p style="font-family:'Proxima Nova', sans-serif;font-size: 30px;margin-top:7px;
                                                                    font-weight: bold;margin-bottom:1px;">13 </p>

                                                                    <p style="font-family:'Proxima Nova', sans-serif;font-size: 17px;margin-top:7px;
                                                                    font-weight: bold;color:#666;">Dni w sprzedaży</p>
                                                                </td>
                                                            </tr>
                                                        </tbody>
                                                    </table>




                                </td>
                            </tr>
                            </tbody></table>

                        <table width="100%" cellspacing="0" cellpadding="0" border="0">
                            <tbody><tr>
                                <td class="sm-pl-18 sm-pr-18" align="center" style="padding:0px 40px 58px 40px;font:16px ;font-family:'Proxima Nova', sans-serif;color:#333333;line-height:32px;">
                                    <hr class="hr_4" style="margin:0px 0 40px 0;">
                                                <p style="font:bold 20px 'Proxima Nova', sans-serif;color:#2EA4FD;line-height:30px;padding:0px 0px 0px 0px;text-align:center;">
                                                    Ostatnio wystawione na sprzedaż
                                                    </p>
                                                    <p style="color: #999; font-weight:bold; margin-top:4px;">Mokotów</p>
                                                    <table style="width: 100%;">
                                                        <tbody style="width:100%;">
                                                            <tr width="100%" dir="ltr">
                                                                <td width="50%" align="center"  class="sm-pl-18 sm-pr-18">
                                                                        <p style="font-family:'Proxima Nova', sans-serif;font-size: 30px;margin-top:7px;
                                                                        font-weight: bold;margin-bottom:1px;">25</p>
                                                                        <p style="font-family:'Proxima Nova', sans-serif;font-size: 17px;margin-top:7px;
                                                                        font-weight: bold;color:#666;">Nowe ogloszenia</p>
                                                                </td>
                                                                <td width="1%" align="center">
                                                                    <div class="vl" style="margin-right:10px;border-left: 1px solid #c9c9c9a8;
                                                                    height: 100px;"></div>
                                                                </td>
                                                                <td width="49%" align="center">
                                                                    <p style="font-family:'Proxima Nova', sans-serif;font-size: 30px;margin-top:7px;
                                                                    font-weight: bold;margin-bottom:1px;">12 500 </p>

                                                                    <p style="font-family:'Proxima Nova', sans-serif;font-size: 17px;margin-top:7px;
                                                                    font-weight: bold;color:#666;">Srednia cena za metr</p>
                                                                </td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                </td>
                            </tr>
                            </tbody></table>
                            <table width="100%" cellspacing="0" cellpadding="0" border="0" >
                                <tbody><tr>
                                    <td class="sm-pl-18 sm-pr-18" align="center" style="padding:40px 60px 40px 60px;font:16px ;font-family:'Proxima Nova', sans-serif;color:#333333;line-height:32px; background-color: #F4F6FA;" >
                                        <p style="font:bold 20px 'Proxima Nova', sans-serif;color:#2EA4FD;line-height:30px;padding:0px 0px 0px 0px;text-align:center;">
                                            Planujesz sprzedać mieszkanie?
                                            </p>
                                            <p style="text-align: center;font-size:17px; color:#555;line-height:28px;margin-top:17px;margin-bottom:17px;">
                                                Zamów bezpłatną wizytę naszego lokalnego eksperta, który przedstawi
                                                Ci ofertę kupna bezpośredniego przez iHomes oraz ofertę współpracy
                                                przy sprzedaży. Doradzimy Ci również jak najkorzystniej sprzedać
                                                mieszkanie oraz przekażemy wskazówki jak podnieść jego wartość.
                                            </p>
                                            <a href="" style="margin-top:10px;text-decoration:none;padding:9px 20px ;background-color:#2EA4FB;color:#ECEEF1; border-radius:10px;">Poproś o ofertę</a>
                                    </td>
                                </tr>
                                </tbody></table>
                            {{-- background-color="0xFF333333" --}}
                        <table width="100%" cellspacing="0" cellpadding="0" border="0">
                            <tbody><tr>
                                <td class="sm-pl-18 sm-pr-18" style="border-top:1px solid #ECEEF1;padding:18px 40px 30px 40px;">
                                    <div style="display:table;width:100%;">
                                        <!--[if mso]><table width="100%" cellspacing="0" cellpadding="0" border="0"><tr><td><![endif]-->

                                        <div class="sm-stack" style="display:table-cell;line-height:16px;font-size:1px;">&nbsp;</div>
                                        <!--[if mso]></td><td><![endif]-->
                                        <div class="sm-stack" style="display:table-cell;vertical-align:middle;">
                                            <table width="100%" cellspacing="0" cellpadding="0" border="0">
                                                <tbody><tr>
                                                    <td align="center">
                                                        <table cellspacing="0" cellpadding="0" border="0" align="left">
                                                            <tbody><tr>
                                                                <td width="14">
                                                                   <a style="color: #999; text-decoration:none;">IHOMES.PL</a>
                                                                </td>

                                                            </tr>
                                                            </tbody></table>
                                                    </td>
                                                </tr>
                                                </tbody></table>
                                        </div>
                                        <!--[if mso]></td><td><![endif]-->
                                        <div class="sm-stack" style="display:table-cell;line-height:16px;font-size:1px;">&nbsp;</div>
                                        <!--[if mso]></td><td><![endif]-->
                                        <div class="sm-stack" style="display:table-cell;vertical-align:middle;">
                                            <table width="100%" cellspacing="0" cellpadding="0" border="0">
                                                <tbody><tr>
                                                    <td class="sm-left" align="right">
                                                        <table cellspacing="0" cellpadding="0" border="0">
                                                            <tbody><tr>
                                                                <td style="padding:0 28px 0 0;">
                                                                    <a href="#" target="_blank"><img src="https://f.hubspotusercontent40.net/hubfs/19888740/Email%20templates/ico-fb.png" width="28" style="display:block;width:100%;max-width:28px;"></a>
                                                                </td>
                                                                <td style="padding:0 28px 0 0;">
                                                                    <a href="#" target="_blank"><img src="https://f.hubspotusercontent40.net/hubfs/19888740/Email%20templates/ico-twt.png" width="28" style="display:block;width:100%;max-width:28px;"></a>
                                                                </td>
                                                                <td>
                                                                    <a href="#" target="_blank"><img src="https://f.hubspotusercontent40.net/hubfs/19888740/Email%20templates/ico-insta.png" width="28" style="display:block;width:100%;max-width:28px;"></a>
                                                                </td>
                                                            </tr>
                                                            </tbody></table>
                                                    </td>
                                                </tr>
                                                </tbody></table>
                                        </div>
                                        <!--[if mso]></td></tr></table><![endif]-->
                                    </div>
                                </td>
                            </tr>
                            </tbody></table>
                    </td>
                </tr>
                </tbody></table>
        </td>
    </tr>
    </tbody></table>

</body></html>

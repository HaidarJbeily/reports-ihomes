<?php
$test = 'sdsad';
?>
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:o="urn:schemas-microsoft-com:office:office"><head>
    <!--[if gte mso 9]><xml>
        <o:OfficeDocumentSettings>
            <o:AllowPNG/>
            <o:PixelsPerInch>96/o:PixelsPerInch
                /o:OfficeDocumentSettings
    </xml><![endif]-->
    <meta charset="UTF-8">
    <title></title>
    <!--[if !mso]>-->
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!--<![endif]-->
    <meta name="format-detection" content="telephone=no">
    <meta name="viewport" content="width=device-width, initial-scale=1.0 maximum-scale=1.0; user-scalable=no;">
    <style>
        .backgroundTable{margin:0 auto;padding:0;width:100%!important;}
        .ReadMsgBody{width:100%;}
        .ExternalClass{width:100%;}
        .ExternalClass,.ExternalClass p,.ExternalClass span,.ExternalClass font,.ExternalClass td,.ExternalClass div{line-height: 100%;}
        body{-webkit-text-size-adjust:100%;-ms-text-size-adjust:100%;width:100%!important;margin:0;padding:0;-webkit-font-smoothing:antialiased!important;}
        table{mso-table-lspace:0pt;mso-table-rspace:0pt;}
        table td{border-collapse: collapse;mso-table-lspace:0pt;mso-table-rspace:0pt;padding:0;}
        img{-ms-interpolation-mode:bicubic;}
        a img{border:none;}
        p{Margin: 0;}
        @media only screen and (max-width: 480px){
            .sm-pl-18{padding-left:18px!important;}
            .sm-pr-18{padding-right:18px!important;}
            .sm-pt-0{padding-top:0!important;}
            .sm-pt-26{padding-top:26px!important;}
            .sm-w-46{width:46px!important;max-width:46px!important;}
            .sm-txt{line-height:auto!important;}
            .sm-txt span{line-height:24px!important;}
            .sm-stack{display:table!important;width:100%!important;}
            .sm-left{float:left!important;}
            .sm-w-100pc{width:100%!important;}
            .center{margin:0 auto!important;}
            .txt-center p{text-align:center!important;}
        }
    </style>
</head>
<body leftmargin="0" marginwidth="0" topmargin="0" marginheight="0" offset="0">
<table width="100%" cellspacing="0" cellpadding="0" border="0">
    <tbody><tr>
        <td>
            <!--[if mso]><table width="660" cellspacing="0" cellpadding="0" border="0" align="center"><![endif]--><!--[if !mso]><!-->
            <table cellspacing="0" cellpadding="0" border="0" align="center" style="width:100%;max-width:660px;"><!--<![endif]-->
                <tbody><tr>
                    <td>
                        <table width="100%" cellspacing="0" cellpadding="0" border="0">
                            <tbody><tr>
                                <td class="sm-pl-18 sm-pr-18" style="padding:26px 40px;">
                                    <table width="100%" cellspacing="0" cellpadding="0" border="0">
                                        <tbody><tr>
                                            <td>
                                                <img src="https://ihomes.pl/wp-content/uploads/2023/10/logoPNG.png" width="102" style="display:block;width:100%;max-width:102px;">
                                            </td>
                                            <td align="right">
                                                <table cellspacing="0" cellpadding="0" border="0">
                                                    <tbody><tr>
                                                        <td class="sm-pr-18" style="padding:0 28px 0 0;">
                                                            <a href="#" target="_blank"><img src="https://ihomes.pl/wp-content/uploads/2023/10/facebook-icon.png" width="28" style="display:block;width:100%;max-width:28px;"></a>
                                                        </td>
                                                        <td class="sm-pr-18" style="padding:0 28px 0 0;">
                                                            <a href="#" target="_blank"><img src="https://ihomes.pl/wp-content/uploads/2023/10/twiter-icon.png" width="28" style="display:block;width:100%;max-width:28px;"></a>
                                                        </td>
                                                        <td>
                                                            <a href="#" target="_blank"><img src="https://ihomes.pl/wp-content/uploads/2023/10/instagram-icon.png" width="28" style="display:block;width:100%;max-width:28px;"></a>
                                                        </td>
                                                    </tr>
                                                    </tbody></table>
                                            </td>
                                        </tr>
                                        </tbody></table>
                                </td>
                            </tr>
                            </tbody></table>
                        <table width="100%" cellspacing="0" cellpadding="0" border="0">
                            <tbody><tr>
                                <td class="sm-pl-18 sm-pr-18" style="padding:16px 40px;background-color:#329af6;">
                                    <div dir="rtl" style="display:table;width:100%;">
                                        <!--[if mso]><table dir="rtl" width="100%" cellspacing="0" cellpadding="0" border="0"><tr><td dir="ltr" align="right"><![endif]-->
                                        <div dir="ltr" class="sm-stack" style="display:table-cell;vertical-align:middle">
                                            <table class="sm-w-100pc" cellspacing="0" cellpadding="0" border="0" align="right">
                                                <tbody><tr>
                                                    <td>
                                                        <img class="center" src="https://ihomes.pl/wp-content/uploads/2023/10/raport-illustration.png" width="125" style="display:block;width:100%;max-width:125px;">
                                                    </td>
                                                </tr>
                                                </tbody></table>
                                        </div>
                                        <!--[if mso]></td><td><![endif]-->
                                        <div class="sm-stack" style="display:table-cell;line-height:24px;font-size:1px;">&nbsp;</div>
                                        <!--[if mso]></td><td dir="ltr"><![endif]-->
                                        <div dir="ltr" class="sm-stack txt-center" style="display:table-cell;vertical-align:middle">
                                            <p style="font:24px Helvetica,Arial,sans-serif;color:#FFFFFF;line-height:30px;"><span style="font-size:24px;font-weight:bold;">Twój raport jest gotowy!</span></p>
                                        </div>
                                        <!--[if mso]></td></table><![endif]-->
                                    </div>
                                </td>
                            </tr>
                            </tbody></table>
                        <table width="100%" cellspacing="0" cellpadding="0" border="0">
                            <tbody><tr>
                                <td class="sm-pl-18 sm-pr-18" style="padding:54px 40px;background-color:#F4F6FA;">
                                    <table width="100%" cellspacing="0" cellpadding="0" border="0">
                                        <tbody><tr>
                                            <td style="font:16px Helvetica,Arial,sans-serif;color:#333333;line-height:32px;">
                                                <p style="font:bold 24px Helvetica,Arial,sans-serif;color:#000000;line-height:30px;padding:0px 0px 23px 0px;">
                                                    Jak oszacowaliśmy wartość mieszkania?
                                                </p>
                                                Prezentowana wartość mieszkania jest precyzyjniejsza niż mógłbyś to zrobić samodzielnie na podstawie dostępnych na rynku ofert. Została ona zbudowana na podstawie <span style="color:#2EA4FB">danych z rynku nieruchomości</span> w najbliższej okolicy. Mogliśmy to zrobić dzięki posiadanym przez nas ofertom sprzedaży praktycznie każdego mieszkania w Twoim sąsiedztwie oraz zaawansowanym algorytmom porównawczym.<br><br>
                                                Jeśli chcesz zobaczyć kilka wybranych <span style="color:#2EA4FB">najbardziej zbliżonych transakcji</span> w Twoim bezpośrednim sąsiedztwie, zgłoś mieszkanie do sprzedaży. Otrzymasz wówczas dwie niezobowiązujące oferty: ofertę bezpośredniego kupna mieszkania oraz ofertę wspólpracy przy sprzedaży.<br>

                                            </td>
                                        </tr>
                                        </tbody></table>
                                </td>
                            </tr>
                            </tbody></table>
                        <table width="100%" cellspacing="0" cellpadding="0" border="0">
                            <tbody><tr>
                                <td class="sm-pl-18 sm-pr-18" style="padding:54px 40px 23px 40px;font:bold 24px Helvetica,Arial,sans-serif;color:#000000;">
                                    Jakie są korzyści z rejestracji konta?
                                </td>
                            </tr>
                            </tbody></table>
                        <table width="100%" cellspacing="0" cellpadding="0" border="0">
                            <tbody><tr>
                                <td class="sm-pl-18 sm-pr-18" style="padding:19px 40px;">
                                    <table width="100%" cellspacing="0" cellpadding="0" border="0">
                                        <tbody><tr>
                                            
                                            <td class="sm-w-46" width="64" valign="top">
                                                <img src="https://ihomes.pl/wp-content/uploads/2023/10/download-icon.png" width="64" style="display:block;width:100%;max-width:64px;">
                                            </td>
                                            <td class="sm-pt-0 sm-pl-18 sm-txt" valign="top" style="padding:2px 0 0 38px;font:16px Helvetica,Arial,sans-serif;color:#333333;line-height:32px;">
                                                Właśnie zarejestrowałeś się na platformie iHomes. Dzięki temu  otrzymujesz darmowy dostęp do panelu użytkownika, w którym znajdziesz wszystkie swoje wyceny mieszkań.
                                            </td>
                                        </tr>
                                        </tbody></table>
                                </td>
                            </tr>
                            </tbody></table>
                        <table width="100%" cellspacing="0" cellpadding="0" border="0">
                            <tbody><tr>
                                <td class="sm-pl-18 sm-pr-18" style="padding:19px 40px;">
                                    <table width="100%" cellspacing="0" cellpadding="0" border="0">
                                        <tbody><tr>
                                            <td class="sm-w-46" width="64" valign="top">
                                                
                                                <img src="https://ihomes.pl/wp-content/uploads/2023/10/pdf-icon.png" width="64" style="display:block;width:100%;max-width:64px;">
                                            </td>
                                            <td class="sm-pt-26 sm-pl-18 sm-txt" valign="top" style="padding:2px 0 0 38px;font:16px Helvetica,Arial,sans-serif;color:#333333;line-height:32px;">
                                                Do każdej wyceny otrzymujesz raport PDF dostępny do pobrania również bezpośrednio z panelu.
                                            </td>
                                        </tr>
                                        </tbody></table>
                                </td>
                            </tr>
                            </tbody></table>
                        <table width="100%" cellspacing="0" cellpadding="0" border="0">
                            <tbody><tr>
                                <td style="padding:32px 0;">
                                    <table width="100%" cellspacing="0" cellpadding="0" border="0">
                                        <tbody><tr>
                                            <td style="border-bottom:1px solid #ECEEF1;line-height:0;font-size:1px;">&nbsp;</td>
                                        </tr>
                                        </tbody></table>
                                </td>
                            </tr>
                            </tbody></table>
                        <table width="100%" cellspacing="0" cellpadding="0" border="0">
                            <tbody><tr>
                                <td class="sm-pl-18 sm-pr-18" style="padding:19px 40px 58px 40px;font:16px Helvetica,Arial,sans-serif;color:#333333;line-height:32px;">
                                    <span style="font-weight:bold;font-size:24px;color:#000000;line-height:24px;">Planujesz sprzedać mieszkanie?</span><br><br>
                                    Zamów bezpłatną wizytę naszego lokalnego eksperta, który przedstawi Ci  <span style="color:#2EA4FB">ofertę kupna bezpośredniego</span> przez iHomes oraz  <span style="color:#2EA4FB">ofertę współpracę przy sprzedaży</span>. Doradzimy Ci również jak najkorzystniej sprzedać mieszkanie oraz przekażemy wskazówki jak podnieść jego wartość.<br>

                                </td>
                            </tr>
                            </tbody></table>
                        <table width="100%" cellspacing="0" cellpadding="0" border="0">
                            <tbody><tr>
                                <td class="sm-pl-18 sm-pr-18" style="border-top:1px solid #ECEEF1;padding:18px 40px 30px 40px;">
                                    <div style="display:table;width:100%;">
                                        <!--[if mso]><table width="100%" cellspacing="0" cellpadding="0" border="0"><tr><td><![endif]-->
                                        <div class="sm-stack" style="display:table-cell;vertical-align:middle;">
                                            <table width="100%" cellspacing="0" cellpadding="0" border="0">
                                                <tbody><tr>
                                                    <td>
                                                        <img src="https://ihomes.pl/wp-content/uploads/2023/10/logoPNG.png" width="55" style="display:block;width:55px;">
                                                    </td>
                                                </tr>
                                                </tbody></table>
                                        </div>
                                        <!--[if mso]></td><td><![endif]-->
                                        <div class="sm-stack" style="display:table-cell;line-height:16px;font-size:1px;">&nbsp;</div>
                                        <!--[if mso]></td><td><![endif]-->
                                        <div class="sm-stack" style="display:table-cell;vertical-align:middle;">
                                            <table width="100%" cellspacing="0" cellpadding="0" border="0">
                                                <tbody><tr>
                                                    <td align="center">
                                                        <table cellspacing="0" cellpadding="0" border="0" align="left">
                                                            <tbody><tr>
                                                                <td width="14">
                                                                    <img src="https://ihomes.pl/wp-content/uploads/2023/10/email-icon-11.png" width="14" style="display:block;width:14px;">
                                                                </td>
                                                                <td style="padding:0 0 0 6px;font:10px Helvetica,Arial,sans-serif;">
                                                                    <a href="mailto:info@ihomes.pl" style="color:#495572;text-decoration:none;">info@ihomes.pl</a>
                                                                </td>
                                                            </tr>
                                                            </tbody></table>
                                                    </td>
                                                </tr>
                                                </tbody></table>
                                        </div>
                                        <!--[if mso]></td><td><![endif]-->
                                        <div class="sm-stack" style="display:table-cell;line-height:16px;font-size:1px;">&nbsp;</div>
                                        <!--[if mso]></td><td><![endif]-->
                                        <div class="sm-stack" style="display:table-cell;vertical-align:middle;">
                                            <table width="100%" cellspacing="0" cellpadding="0" border="0">
                                                <tbody><tr>
                                                    <td class="sm-left" align="right">
                                                        <table cellspacing="0" cellpadding="0" border="0">
                                                            <tbody><tr>
                                                                <td style="padding:0 28px 0 0;">
                                                                    <a href="#" target="_blank"><img src="https://ihomes.pl/wp-content/uploads/2023/10/facebook-icon.png" width="28" style="display:block;width:100%;max-width:28px;"></a>
                                                                </td>
                                                                <td style="padding:0 28px 0 0;">
                                                                    <a href="#" target="_blank"><img src="https://ihomes.pl/wp-content/uploads/2023/10/twiter-icon.png"  width="28" style="display:block;width:100%;max-width:28px;"></a>
                                                                </td>
                                                                <td>
                                                                    <a href="#" target="_blank"><img src="https://ihomes.pl/wp-content/uploads/2023/10/instagram-icon.png"  width="28" style="display:block;width:100%;max-width:28px;"></a>
                                                                </td>
                                                            </tr>
                                                            </tbody></table>
                                                    </td>
                                                </tr>
                                                </tbody></table>
                                        </div>
                                        <!--[if mso]></td></tr></table><![endif]-->
                                    </div>
                                </td>
                            </tr>
                            </tbody></table>
                    </td>
                </tr>
                </tbody></table>
        </td>
    </tr>
    </tbody></table>

</body></html>
